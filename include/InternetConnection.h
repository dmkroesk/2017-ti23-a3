//
// Created by bartm on 20-2-2017.
//

#ifndef INTERNETRADIOPROJECT_INTERNETCONNECTION_H
#define INTERNETRADIOPROJECT_INTERNETCONNECTION_H
#include <sys/socket.h>

#include <stdio.h>
    // connect dynamic to internet
    char *ConnectToInternet(void);
    // connect static to internet
    char * ConnectToInternetStatic(void);
    // check internet connection
    int * ConnectedToInternet(void);
    // host tcp server and return every time response
    void TCPServer(char * response);

    //  create stream
    FILE * CreateStream(TCPSOCKET * socket);

    // connect to server
    TCPSOCKET *ConnectClient(char *address, int port);
    // host server and accept tcp clients
    TCPSOCKET *ConnectServer(int port);
    //  send text to socket
    void SendTextToSocket(TCPSOCKET *socket, char *text);
    // close socket
    void CloseSocket(TCPSOCKET *socket);

    //  menu items
    void internetOkCommand(void);
    void internetIPCommand(void);
    void internetConnectCommand(void);
    void internetDynamicCommand(void);
    void internetStaticCommand(void);
    void internetTestCommand(void);


#endif //INTERNETRADIOPROJECT_INTERNETCONNECTION_H
