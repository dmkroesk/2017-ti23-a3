//
// Created by Sascha on 24-3-2017.
//

#ifndef SASCHA2_STATIONFLASH_H
#define SASCHA2_STATIONFLASH_H

#include "../include/radioStations.h"
int addStation(STATION station);
int removeLastStation();
int addStream(char desc, char url, char path, int port);
STATION * getStations();
int saveStations();
int loadStations();
void setAST(int ast);
void showPage(u_long pgn);
void printStations();
#endif //SASCHA2_STATIONFLASH_H
