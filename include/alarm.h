/* ========================================================================
 * Authors:         Cas Koopmans
 *                  Justin Boomen
 * Date:            20/02/2017
 * last modified:   20/02/2017
 * ======================================================================== */

#define MAX_PATH_LENGTH 30
#include <time.h>

#define LISTLENGTH 20
#define SNOOZETIME 10
#define SNOOZECOUNT 3

#define DAILY 0
#define WEEKLY 1
#define FIRSTLINE 0
#define SECONDLINE 1
#define HOURPOSITION 0
#define MINUTEPOSITION 1
#define HOURSELECT 6
#define MINUTESELECT 9
#define EDIT 0
#define DELETE 1
#define ADD 0
#define SET 1

//struct for alarm containing varibles for date, weekly and daily repeats snooze count and an eventual sound path.
struct _alarm{
    int timeHour;
    int timeMinutes;
    int checkTimeHour;
    int checkTimeMinutes;
    int repeatDaily;
    int repeatWeekly;
    int dayOfWeek;
    int snoozeCount;
    char soundPath[MAX_PATH_LENGTH];
};

//struct so the arg for timerthread is only 1. It contains the time and alarm.
typedef struct _timer{
    struct _alarm alarm;
    struct _tm time;
}TIMER;

int getAlarmState(void);
void setAlarmState(int);

void InitAlarmArray(void);
void AddAlarm(struct _alarm*);
void ShowAlarms(void);
void RemoveAlarm(int n);
struct _alarm GetAlarm(int n);
struct _alarm GetAllAlarms(void);

void AddAlarm(struct _alarm*);
void RemoveAlarm(int);
void SetAlarm(int, int, int, char*, int,int,int);
struct _alarm MakeOneTimeAlarm(int, int, char*);
struct _alarm MakeDailyAlarm(int, int, char*, int);
struct _alarm MakeWeeklyAlarm(int, int, char*, int, int);
void CheckAlarm(struct _alarm*, struct _tm*);
void ShowAlarms(void);
void Snooze(struct _alarm*);
void Stop(struct _alarm*);

void StartTimerThread(struct _alarm*, tm*);
void TwoAlarms(tm*);
void StartArrayThread(tm*);
void ArrayAlarms(tm*);

void EditAlarmOkCommand();
void EditSpecAlarmOkCommand();
void alarmOkCommand(void);
void AddAlarmOkCommand(void);
void EditAlarmOkCommand(void);
void DeleteAlarmOkCommand(void);
void DisplayAlarmArray(int);
void EditAlarm(void);
void DeleteAlarm(void);
void OneTime(int);
void OneTimeAlarmOkCommand(void);
void SetOneTimeAlarmOkCommand(void);
void DailyAlarmOkCommand(void);
void SetDailyAlarmOkCommand(void);
void WeeklyAlarmOkCommand(void);
void SetWeeklyAlarmOkCommand(void);
void SetTime(int, int, int*, int*);
void SetIteration(int, int);
void NumIterationOkCommand(void);
void SetNumIterationOkCommand(void);
void NumIteration(int);
void subAlarmOptionESCCommand(void);

/*  ����  End Of File  �������� �������������������������������������������� */

