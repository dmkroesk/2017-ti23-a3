//
// Created by gijsb on 20-2-2017.
//

#ifndef CODE_MENU_H
#define CODE_MENU_H
#define MAX_CHAR_SIZE 16

struct MenuItem{
    struct MenuItem *next;
    char commandName[MAX_CHAR_SIZE];
    void (*commandOk)(void);
    void (*commandESC) (void);
    struct MenuItem *prev;
};

struct MenuItem *selectedItem;
struct MenuItem *headItem;
struct MenuItem *lastItem;
void viewTimeZone();
void menuInit(void);
void addMenuItem(void(*)(void), void(*) (void), char*);
void menuPrev(void);
void menuNext(void);
void timeOkCommand(void);
void showSelected(void);
void subOptionsESCCommand(void);
void onlineTimeOkCommand(void);
void optionsESCCommand(void);
int showMenu(void);
void pressedStream();
void stopStream();
void switchStream();
void playStreamSwitch();

int inMenu;
#endif //CODE_MENU_H
