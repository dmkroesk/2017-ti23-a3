//Initialize the html site.
//Used for testing JSon
function init()
{
	var myJSON = '{ "stream1":"NA",	"stream2":"NA", "stream3":"NA", "playingstream":"NA", "volume": 10 }';
	var myObj = JSON.parse(myJSON);
	document.getElementById("container").innerHTML = "Playing: " + myObj.playingstream + "<br>" +
													 "<br>" +
													 "Stream: " + myObj.stream1 + "<br>" +
													 "Stream: " + myObj.stream2 + "<br>" +
													 "Stream: " + myObj.stream3 + "<br>" +
													 "<br>" +
													 "Volume: " + myObj.volume + "<br>";
}

//You can send a json struct here for the javascript to handle.
function updateJson(myJSON)
{
	var myObj = JSON.parse(myJSON);
	document.getElementById("container").innerHTML = "Playing: " + myObj.playingstream + "<br>" +
													 "<br>" +
													 "Stream: " + myObj.stream1 + "<br>" +
													 "Stream: " + myObj.stream2 + "<br>" +
													 "Stream: " + myObj.stream3 + "<br>" +
													 "<br>" +
													 "Volume: " + myObj.volume + "<br>";
}
