/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */

#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "menu.h"
#include "alarm.h"
#include <time.h>
#include "rtc.h"
#include "realTime.h"
#include "InternetConnection.h"
#include "IPStream.h"
#include "LedDisplay.h"
#include "../include/display.h"
#include "../include/realTime.h"
#include "settings.h"
#include "../include/radioStations.h"
#include "stateMachine.h"
#include "VolumeHandler.h"
#include "Station.h"
#include "HostWebsite.h"

#include "../include/StationFlash.h"
#include "../include/flashSettings.h"

#define  AMOUNT_OF_SETTINGS     0
#include "../include/Station.h"

/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
 * \addtogroup System
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* ����������������������������������������������������������������������� */
/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{
    /*
     *  scan for valid keys AND check if a MMCard is inserted or removed
     */
    //printf("MainBeat\n");
    KbScan();
    CardCheckCard();
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif //USE_JTAG

    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
    int nError = 0;

    if (OnOff==ON)
    {
        nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
        if (nError == 0)
        {
            init_8_bit_timer();
        }
    }
    else
    {
        // disable overflow interrupt
        disable_8_bit_timer_ovfl_int();
    }
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Main entry of the SIR firmware
 *
 * All the initialisations before entering the for(;;) loop are done BEFORE
 * the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
 * initialisatons need to be done again when leaving the Setup because new values
 * might be current now
 *
 * \return \b never returns
 */

/* ����������������������������������������������������������������������� */
int main(void) {

    WatchDogDisable();
    NutDelay(100);

    //Init functions
    SysInitIO();
    SPIinit();
    LedInit();
    Uart0DriverInit();
    Uart0DriverStart();
    LogInit();
    CardInit();
    At45dbInit(); //FLASH MEMORY
    RcInit();
    KbInit();
    LcdLowLevelInit();

    if(0 == checkSettings() ) {
        loadSettingsFromFlash();
        loadSettings();}

    //getting time
    tm gmt;
    X12Init();
    if (X12RtcGetClock(&gmt) == 0) {
        if (getTime(&gmt) == 0) {
            LogMsg_P(LOG_INFO, PSTR("RTC time [%02d:%02d:%02d]"), gmt.tm_hour, gmt.tm_min, gmt.tm_sec);
        }
    }
    gmt.tm_wday = getWeekday(gmt.tm_mday, gmt.tm_mon + 1, gmt.tm_year + 1900);

    sei();

//    InitAlarmArray();
//    ArrayAlarms(&gmt);

    char *ip = ConnectToInternet();
    SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt
    StartLedThread();
    //Add Settings in this function -> userData.c
    //handleInit();
    //StartMultiThreaded(ConnectToIPStreamRadio(&stations[0])); // start player

    NutSleep(200);
    //initSettings();

//    if(getAmountofSettings < AMOUNT_OF_SETTINGS)
//    {
//        //ADD ALL SETTINGS WITH DEFAULT VALUES
//        addSetting("tmz", 1, setTimeZone);
//        printf("ADDED");
//    }

    //addSetting("tms", 0, setTimeSync);
    //loadSettings();
    //LcdLoadCustomCharacters();

    StartMultiThreaded(ConnectToIPStreamRadio(&stations[0])); // start player
    startWebThread();

    NutSleep(200);
    //initSettings();

//    if(getAmountofSettings < AMOUNT_OF_SETTINGS)
//    {
//        //ADD ALL SETTINGS WITH DEFAULT VALUES
//        addSetting("tmz", 1, setTimeZone);
//        printf("ADDED");
//    }

    //addSetting("tms", 0, setTimeSync);
    //loadSettings();
    //hostRadioWebsite(ip);
    printf("SASCHA DID NOT CAUSE CRASH");
    //LcdLoadCustomCharacters();
    
    ClearDisplay();
    DisplayTime(gmt.tm_hour, gmt.tm_min);

    stateLoop();

    return (0);      // never reached, but 'main()' returns a non-void, so.....
}
/* ---------- end of module ------------------------------------------------ */
