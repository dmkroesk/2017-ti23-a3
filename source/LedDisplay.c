//
// Created by gijsb on 7-3-2017.
//

#include "LedDisplay.h"
#include "keyboard.h"
#include "display.h"
#include "menu.h"


#include <sys/timer.h>
#include <sys/event.h>
#include <dev/irqreg.h>

static int isLightOn = 0;

//BackLightLedStatus
u_char BLLS = LED_STOP;

/*!
 * easy function for turning the backlightled on.
 */
void ledOn(){
    LcdBackLight(LCD_BACKLIGHT_ON);
}

/*!
 * easy function for turning the backlightled off.
 */
void ledOff(){
    LcdBackLight(LCD_BACKLIGHT_OFF);
}

/*!
 * this function turns on blinking.
 */
void blinkOn(){
    BLLS = LED_BLINK;
}

/*!
 * this function turns the blinking off.
 */
void blinkOff(){
    BLLS = LED_STOP;
    ledOff();
}

void backLightBlink() {
    if(!isLightOn){
        ledOn();
        isLightOn = 1;
    }
    else
    {
        ledOff();
        isLightOn = 0;
    }
}

/*!
 * function that turns the backlightled on for 10 seconds. This is used by the alt button.
 */
void tenSecOn(){
    ledOn();
    BLLS = LED_ON;
    NutSleep(10000);
}

void CheckLight()
{
    switch(BLLS)
    {
        case LED_BLINK:
            backLightBlink();
            NutSleep(250);
            break;
        case LED_ON:
            ledOff();
            BLLS = LED_STOP;
            break;
        case LED_STOP:
            break;
        default:
            printf("BackLedLightStatus is not right: %d\n", BLLS);
        break;
    }
}

u_char returnBLLS(){
    return BLLS;
}

void AltBackLightPush(){
    if(keypressed() == KEY_ALT & returnBLLS != LED_BLINK){
        tenSecOn();
    }
}

THREAD(LedThread, args) {
    NutThreadSetPriority(100);
    for(;;){
        AltBackLightPush();
        CheckLight();
        NutSleep(500);
    }
}

void StartLedThread()
{
    NutThreadCreate("BackLed", LedThread, NULL, 512);
    printf("\nLed thread started");
}


