/* ========================================================================
* Authors:         Cas Koopmans
*                  Justin Boomen
* Date:            20/02/2017
* last modified:   20/02/2017
* ======================================================================== */
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <keyboard.h>


#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include "display.h"
#include "realTime.h"
#include "alarm.h"
#include "menu.h"
#include "LedDisplay.h"
#include "radioStations.h"

int alarmTriggered = 0;
int ESC;
int selectedAlarmIndex;
int getAlarmState(){ return  alarmTriggered;}
void setAlarmState(int newAlarm){alarmTriggered = newAlarm;}

//begin alarm list-------------------------------------------------
static int alarmsInList = 0;
static struct _alarm *alarm_list;

int streamStarted = 0;
//struct _alarm alarmlist[LISTLENGTH];

/*!
 * \brief Initializing the alarm by allocating the memory for the max size of the alarms.
 */
void InitAlarmArray(){
    alarm_list = (struct _alarm*) malloc(sizeof(struct _alarm) * LISTLENGTH);
    int i;
    struct _alarm alarm;
    for(i = 0; i < LISTLENGTH; i++){
        alarm_list[i] = alarm;
    }
}

/*!
 * \brief Adds an alarm to the memorylist
 *
 * \param alarm     the alarm struct you want to add
 */
void AddAlarm(struct _alarm *alarm){
    if(alarmsInList <= LISTLENGTH) {
        alarm_list[alarmsInList] = *alarm;
        alarmsInList++;
    }
}

/*!
 * \brief A test method for printing out the time in minutes and hours for every _alarm struct in the alarm_list array.
 */
void ShowAlarms(){
    int i;
    for(i = 0; i < 4; i++){
        printf("ALARM time [%02d:%02d]\n", alarm_list[i].timeHour, alarm_list[i].timeMinutes);
    }
}

/*!
 * \brief Removes an alarm from the memorylist
 *
 * \param n   the place where you want to remove the alarm in the array
 */
void RemoveAlarm(int n){

    if(n >= 0 && n <= LISTLENGTH){
        int i;
        for(i = n; i<= alarmsInList; i++) {
            alarm_list[n] = alarm_list[n + 1];
        }
        alarmsInList--;
    }
}
/*!
 * \brief Gets all alarms in the alarm_list
 *
 */
struct _alarm GetAllAlarms(){
    return *alarm_list;
}

/*!
 * \brief Gets a specific alarm from the alarm_list
 *
 * \param n   the place in the array where the alarm is you want
 */

struct _alarm GetAlarm(int n){
    return alarm_list[n];
}

/*!
 * \brief Changes an existing alarm with the given parameters and alarm struct
 *
 * \param alarmindex    the index of the alarm struct you want to set
 * \param hours         the time of the day in hours you want to set the alarm
 * \param minutes       the time of the day in minutes you want to set the alarm
 * \param sound         the filepath to the sound(effect)
 * \param repeatDaily   A boolean to see if the alarm has to be removed or repeated daily. 1 is daily 0 is one time.
 * \param repeatWeekly  A boolean to see if the alarm has to be removed or repeated weekly. 1 is daily 0 is one time.
 * \param dayOfWeek     An int to set the date the weekly alarm has to go off (a value from 0-6 - 0 sunday 1 monday etc).
 */
void SetAlarm(int alarmIndex, int hours, int minutes, char *sound, int repeatDaily, int repeatWeekly, int dayOfWeek) {
    alarm_list[alarmIndex].timeHour = hours;
    alarm_list[alarmIndex].timeMinutes = minutes;
    alarm_list[alarmIndex].checkTimeHour = hours;
    alarm_list[alarmIndex].checkTimeMinutes = minutes;
    strcpy(alarm_list[alarmIndex].soundPath, sound);
    alarm_list[alarmIndex].repeatDaily = repeatDaily;
    alarm_list[alarmIndex].repeatWeekly = repeatWeekly;
    alarm_list[alarmIndex].dayOfWeek = dayOfWeek;
    printf("Alarm \"%s\" is changed: new time: %02d:%02d \n", alarm_list[alarmIndex].soundPath,
           alarm_list[alarmIndex].timeHour, alarm_list[alarmIndex].timeMinutes);
}

/*!
 * \brief Makes a new alarm with the given parameters.
 *
 * \param hours         the time of the day in hours you want to set the alarm
 * \param minutes       the time of the day in minutes you want to set the alarm
 * \param sound         the filepath to the sound(effect)
 */
struct _alarm MakeOneTimeAlarm(int hours, int minutes, char *sound){
    struct _alarm alarm;
    alarm.timeHour = hours;
    alarm.timeMinutes = minutes;
    alarm.checkTimeHour = hours;
    alarm.checkTimeMinutes = minutes;
    strcpy(alarm.soundPath, sound);
    alarm.snoozeCount = SNOOZECOUNT;
    alarm.repeatDaily = -1;
    alarm.repeatWeekly = -1;
    alarm.dayOfWeek = -1;
    printf("Alarm \"%s\" is made for %02d:%02d\n", alarm.soundPath, hours, minutes);
    return alarm;
}

/*!
 * \brief Makes a new alarm with the given parameters.
 *
 * \param hours         the time of the day in hours you want to set the alarm
 * \param minutes       the time of the day in minutes you want to set the alarm
 * \param sound         the filepath to the sound(effect)
 * \param repeatDaily   An integer to see how many days the alarm has to go off
 */
struct _alarm MakeDailyAlarm(int hours, int minutes, char *sound, int repeatDaily){
    struct _alarm alarm;
    alarm.timeHour = hours;
    alarm.timeMinutes = minutes;
    alarm.checkTimeHour = hours;
    alarm.checkTimeMinutes = minutes;
    strcpy(alarm.soundPath, sound);
    alarm.snoozeCount = SNOOZECOUNT;
    alarm.repeatDaily = repeatDaily;
    alarm.repeatWeekly = -1;
    alarm.dayOfWeek = -1;
    printf("Alarm \"%s\" is made for %02d:%02d coming %i days\n", alarm.soundPath, hours, minutes, repeatDaily);
    return alarm;
}

/*!
 * \brief Makes a new alarm with the given parameters.
 *
 * \param hours         the time of the day in hours you want to set the alarm
 * \param minutes       the time of the day in minutes you want to set the alarm
 * \param sound         the filepath to the sound(effect)
 * \param repeatWeekly  A boolean to see how many weeks the alarm has to go off
 * \param dayOfWeek     An int to set the date the weekly alarm has to go off (a value from 0-6 - 0 sunday 1 monday etc).
 */
struct _alarm MakeWeeklyAlarm(int hours, int minutes, char *sound, int repeatWeekly, int dayOfWeek){
    struct _alarm alarm;
    alarm.timeHour = hours;
    alarm.timeMinutes = minutes;
    alarm.checkTimeHour = hours;
    alarm.checkTimeMinutes = minutes;
    strcpy(alarm.soundPath, sound);
    alarm.snoozeCount = SNOOZECOUNT;
    alarm.repeatDaily = -1;
    alarm.repeatWeekly = repeatWeekly;
    alarm.dayOfWeek = dayOfWeek;
    printf("Alarm \"%s\" is made for %02d:%02d coming %i weeks day %i\n", alarm.soundPath, hours, minutes, repeatWeekly, dayOfWeek);
    return alarm;
}

/*!
 * \brief checks if the time in the given alarm is the same as
 *        the time that is given
 *
 * \param alarm     the alarm struct you want to check
 * \param tm        the time you want to check the alarm with
 */
void CheckAlarm(struct _alarm *alarm, struct _tm *tm){
    if(alarm->checkTimeHour == tm->tm_hour && alarm->checkTimeMinutes == tm->tm_min){
        printf("Alarm \"%s\" is ringing! \n", alarm->soundPath);
        if(!streamStarted) {
            StartMultiThreaded(ConnectToIPStream("145.58.52.145", 80, "/3fm-bb-mp3")); // start player
            streamStarted = 1;
        }

        if((alarm->repeatWeekly >= 0) && (alarm->dayOfWeek == tm->tm_wday)){
            printf("Alarm \"%s\" is ringing! \n", alarm->soundPath);
            --alarm->repeatWeekly;
        }else if(alarm->repeatDaily >= 0){
            printf("Alarm \"%s\" is ringing! \n", alarm->soundPath);
            --alarm->repeatDaily;
        }else{
            printf("Not correct day");
        }
    }
}

/*!
 * \brief Lets you snooze the given alarm with a variable
 *        snooze time
 *
 *        after a SNOOZECOUNT times snoozing it will automatically
 *        stop the alarm with the Stop(struct _alarm *alarm)
 *        function
 *
 * \param alarm     the alarm struct you want to snooze
 */
void Snooze(struct _alarm *alarm){
    if(alarm->snoozeCount != 0) {
        printf("Before snooze: %d:%d \n", alarm->checkTimeHour, alarm->checkTimeMinutes);
        alarm->checkTimeMinutes += SNOOZETIME;
        if (alarm->checkTimeMinutes >= 60) {
            alarm->checkTimeMinutes -= 60;
            alarm->checkTimeHour++;
            if (alarm->checkTimeHour == 24) {
                alarm->checkTimeHour = 0;
            }
        }
        printf("Snoozed by %d minutes \n", SNOOZETIME);
        streamStarted = 0;
        printf("After snooze: %d:%d \n", alarm->checkTimeHour, alarm->checkTimeMinutes);
        alarm->snoozeCount--;
    }
    else{
        Stop(alarm);}
}

/*!
 * \brief Lets you stop the given alarm and puts it's checktime back
 *        to the time it had set.
 *
 * \param alarm     the alarm struct you want to stop
 */
void Stop(struct _alarm *alarm){
    printf("Before stop: %d:%d \n", alarm->checkTimeHour, alarm->checkTimeMinutes);
    alarm->checkTimeHour = alarm->timeHour;
    alarm->checkTimeMinutes = alarm->timeMinutes;
    printf("After stop: %d:%d \n", alarm->checkTimeHour, alarm->checkTimeMinutes);
    StopPlaying();
}

/*!
 * Calls the check alarm method for the given _alarm struct.
 * @param args containing a tm struct which will contain the current date and time and a _alarm struct containing a date.
 * @return void
 */
THREAD(TimerThread, args) {
TIMER *timer = (TIMER *) args;
struct _alarm *alarmT = &timer->alarm;
tm *gmt = &timer->time;
printf("\nThread Started\n");
//NutSleep(60010 - gmt->tm_sec);
for (;;) {
NutSleep(1000);
if (getTime(gmt)== 0){
printf("RTC time [%02d:%02d]\n", gmt->tm_hour, gmt->tm_min);
}
printf("ALARM time [%02d:%02d]\n", alarmT->timeHour, alarmT->timeMinutes);
CheckAlarm(alarmT, gmt);
//NutSleep(60000);
}
}

/*!
 * Calls the check alarm method for every alarm in the array.
 * @param args containing a tm struct which will contain the current date and time.
 * Also makes use of a static _alarm alarm_list array and a static int alarmsInList variable containing the size.
 * @return void
 */
THREAD(ArrayThread, args) {
tm *gmt = (tm *) args;
printf("\nThread Started\n");
NutSleep(60010 - gmt->tm_sec);
for (;;) {
if (getTime(gmt)== 0){
printf("RTC time [%02d:%02d]\n", gmt->tm_hour, gmt->tm_min);
}
int i;
printf("%i\n", alarmsInList);
for(i = 0; i < alarmsInList; i++){
printf("ALARM time [%02d:%02d]\n", alarm_list[i].timeHour, alarm_list[i].timeMinutes);
printf("\n %i\n", i);
CheckAlarm(&alarm_list[i], gmt);
NutSleep(10);
}
NutSleep(60000);
}
}

/*!
 * Starts the TimerThread.
 * @return void
 */
void StartTimerThread(struct _alarm *alarm, tm *gmt) {
    TIMER *ti = malloc (sizeof (TIMER));
    ti->time = *gmt;
    ti->alarm = *alarm;
    NutThreadCreate("Timer", TimerThread, ti, 512);
}

/*!
 * Starts the ArrayThread.
 * @return void
 */
void StartArrayThread(tm *gmt){
    NutThreadCreate("Array", ArrayThread, gmt, 512);
}

/*!
 * Test method to test multiple alarms running on a seperate thread.
 * @param tm *gmt A tm struct containing the current date and time
 * @return void
 */
void ArrayAlarms(tm *gmt){
    struct _alarm alarm1 = MakeOneTimeAlarm(20, 33, "later1");
    struct _alarm alarm2 = MakeOneTimeAlarm(21, 00, "later2");
    struct _alarm alarm3 = MakeOneTimeAlarm(21, 30, "later3");
    struct _alarm alarm4 = MakeOneTimeAlarm(21, 49, "later4");
    AddAlarm(&alarm1);
    AddAlarm(&alarm2);
    AddAlarm(&alarm3);
    AddAlarm(&alarm4);
    ShowAlarms();
    StartArrayThread(gmt);
}

/*!
 * \brief Menu to open after selecting alarm in main menu, choose to add, set or delete an alarm
 */
void alarmOkCommand(){
    menuInit();

    addMenuItem(AddAlarmOkCommand, subOptionsESCCommand, "Add alarm");
    //addMenuItem(SetAlarmOkCommand, subOptionsESCCommand, "Set alarm");
    addMenuItem(EditAlarmOkCommand, subOptionsESCCommand, "Edit alarm");
    addMenuItem(DeleteAlarmOkCommand, subOptionsESCCommand, "Delete alarm");
    showSelected();
}

/*!
 * \brief Add an alarm, choose once, daily or weekly
 */
void AddAlarmOkCommand(){
    menuInit();

    addMenuItem(OneTimeAlarmOkCommand, alarmOkCommand, "Once");
    addMenuItem(DailyAlarmOkCommand, alarmOkCommand, "Daily");
    addMenuItem(WeeklyAlarmOkCommand, alarmOkCommand, "Weekly");

    showSelected();
}

void EditSpecAlarmOkCommand(){
    menuInit();

    addMenuItem(OneTimeAlarmOkCommand, EditAlarmOkCommand, "Once");
    addMenuItem(DailyAlarmOkCommand, EditAlarmOkCommand, "Repeat daily");
    addMenuItem(WeeklyAlarmOkCommand, EditAlarmOkCommand, "Repeat weekly");
    showSelected();
}

/*!
 * \brief code for confirming your alarm.
 *
 */
void EditAlarmOkCommand(){
    DisplayAlarmArray(EDIT);
}

/*!
 * \brief when you choose for deleting the selected alarm.
 *
 */
void DeleteAlarmOkCommand() {
    DisplayAlarmArray(DELETE);
}

/*!
 * \brief Display array of alarms in menu for editing or deleting
 * @param setOrDelete 0 for edit (EDIT), 1 for delete (DELETE)
 */
void DisplayAlarmArray(int editOrDelete){
    if(NULL != alarm_list[0].timeHour) {
        menuInit();
        int i;
        char alarmIndex[7];
        for(i = 0; i < alarmsInList; i++)
        {
            sprintf(alarmIndex, "Alarm %i", i + 1);
            if(editOrDelete == EDIT){
                addMenuItem(EditAlarm, alarmOkCommand, alarmIndex);
            }
            if(editOrDelete == DELETE){
                addMenuItem(DeleteAlarm, alarmOkCommand, alarmIndex);
            }
        }
        showSelected();
    }
}

/*!
 * \brief Edit existing alarm from alarm array (includes setting one time, daily or weekly)
 */
void EditAlarm(){
    menuInit();

    char alarmName[8];
    strcpy(alarmName, selectedItem->commandName);
    int alarmIndexToEdit;
    sscanf(alarmName, "Alarm %i", &alarmIndexToEdit);
    selectedAlarmIndex = (alarmIndexToEdit - 1);

    addMenuItem(SetOneTimeAlarmOkCommand, EditAlarmOkCommand, "Once");
    addMenuItem(SetDailyAlarmOkCommand, EditAlarmOkCommand, "Daily");
    addMenuItem(SetWeeklyAlarmOkCommand, EditAlarmOkCommand, "Weekly");

    showSelected();
}

/*!
 * \brief Delete existing alarm from alarm array
 */
void DeleteAlarm(){
    char alarmName[8];
    strcpy(alarmName, selectedItem->commandName);
    int alarmIndexToDelete;
    sscanf(alarmName, "Alarm %i", &alarmIndexToDelete);
    RemoveAlarm(alarmIndexToDelete - 1);
    inMenu = 0;
}

/*!
 * \brief Set time for new one time alarm
 */
void OneTimeAlarmOkCommand() {
    OneTime(ADD);
}

/*!
 * \brief Set time for existing one time alarm
 */
void SetOneTimeAlarmOkCommand() {
    OneTime(SET);
}

/*!
 * \brief Setting time for new or existing one time alarm
 * @param addOrSet 0 for add (ADD), 1 for set (SET)
 */
void OneTime(int addOrSet) {
    menuInit();
    ClearDisplay();
    int h, m;
    struct _alarm a;
    if(addOrSet == ADD) {
        SetTime(8, 0, &h, &m);
        a = MakeOneTimeAlarm(h, m, "");
        AddAlarm(&a);
    }
    if(addOrSet == SET) {
        SetTime(alarm_list[selectedAlarmIndex].timeHour, alarm_list[selectedAlarmIndex].timeMinutes, &h, &m);
        a = GetAlarm(selectedAlarmIndex);
        SetAlarm(selectedAlarmIndex, h, m, "", -1, -1, -1);
    }
    inMenu = 0;
}

/*!
 * \brief Select number of daily iterations for new alarm
 */
void DailyAlarmOkCommand() {
    SetIteration(ADD, DAILY);
}

/*!
 * \brief Select number of daily iterations for existing alarm
 */
void SetDailyAlarmOkCommand() {
    SetIteration(SET, DAILY);
}

/*!
 * \brief Select number of weekly iterations for new alarm
 */
void WeeklyAlarmOkCommand() {
    SetIteration(ADD, WEEKLY);
}

/*!
 * \brief Select number of weekly iterations for existing alarm
 */
void SetWeeklyAlarmOkCommand() {
    SetIteration(SET, WEEKLY);
}

/*!
 * \brief Selecting number of iterations daily or weekly for existing or new alarm
 * @param addOrSet 0 for add (ADD), 1 for set (SET)
 * @param dayOrWeek 0 for day (DAILY), 1 for week (WEEKLY)
 */
void SetIteration(int addOrSet, int dayOrWeek) {
    menuInit();

    if(addOrSet == ADD) {
        if(DAILY == dayOrWeek){
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "2 days");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "3 days");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "4 days");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "5 days");
        }
        if(WEEKLY == dayOrWeek){
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "2 weeks");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "3 weeks");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "4 weeks");
            addMenuItem(NumIterationOkCommand, AddAlarmOkCommand, "5 weeks");
        }
    }
    if(addOrSet == SET) {
        if(DAILY == dayOrWeek){
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "2 days");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "3 days");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "4 days");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "5 days");
        }
        if(WEEKLY == dayOrWeek){
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "2 weeks");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "3 weeks");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "4 weeks");
            addMenuItem(SetNumIterationOkCommand, EditAlarmOkCommand, "5 weeks");
        }
    }

    showSelected();
}

/*!
 * \brief Set time for amount of iterations for new alarm
 */
void NumIterationOkCommand() {
    NumIteration(ADD);
}

/*!
 * \brief Set time for amount of iterations for existing alarm
 */
void SetNumIterationOkCommand() {
    NumIteration(SET);
}

/*!
 * \brief Setting time for selected amount of daily/ weekly iterations for existing or new alarm
 * @param addOrSet 0 for add (ADD), 1 for set (SET)
 */
void NumIteration(int addOrSet) {
    int h;
    int m;
    char name[10];
    strcpy(name, selectedItem->commandName);
    struct _alarm a;

    if (strstr(name, "days") != NULL) {
        int daysToIterate;
        sscanf(name, "%i days", &daysToIterate);
        if(addOrSet == ADD) {
            SetTime(8, 0, &h, &m);
            a = MakeDailyAlarm(h, m, "", daysToIterate);
            AddAlarm(&a);
        }
        if(addOrSet == SET) {
            SetTime(alarm_list[selectedAlarmIndex].timeHour, alarm_list[selectedAlarmIndex].timeMinutes, &h, &m);
            a = GetAlarm(selectedAlarmIndex);
            SetAlarm(selectedAlarmIndex, h, m, "", daysToIterate, -1, -1);
        }
        inMenu = 0;
    }
    if (strstr(name, "weeks") != NULL) {
        int weeksToIterate;
        sscanf(name, "%i days", &weeksToIterate);
        if(addOrSet == ADD) {
            SetTime(8, 0, &h, &m);
            a = MakeWeeklyAlarm(h, m, "", weeksToIterate, 2);
            AddAlarm(&a);
        }
        if(addOrSet == SET) {
            SetTime(alarm_list[selectedAlarmIndex].timeHour, alarm_list[selectedAlarmIndex].timeMinutes, &h, &m);
            a = GetAlarm(selectedAlarmIndex);
            SetAlarm(selectedAlarmIndex, h, m, "", -1, weeksToIterate, 2);
        }
        inMenu = 0;
    }
}

/*!
 * \brief Setting time
 * @param h hours to start with
 * @param m minutes to start with
 * @param hOutput returnvalue of hours
 * @param mOutput returnvalue of minutes
 */
void SetTime(int h, int m, int *hOutput, int *mOutput) {
    int hour = h;
    int minute = m;
    int cursorPosition = HOURPOSITION;
    int ispressed = 1;
    ClearDisplay();
    DisplayTime(h, m);
    DrawArrow(HOURSELECT);
    SetCursor(FIRSTLINE, HOURSELECT);

    int inTimeSelection = 1;

    while (inTimeSelection) {
        if ((keypressed() == KEY_RIGHT) & (!ispressed)) {
            if (cursorPosition != 1) {
                SetCursor(SECONDLINE, HOURSELECT);
                LcdChar(' ');
                cursorPosition = MINUTEPOSITION;
                DrawArrow(MINUTESELECT);
                SetCursor(FIRSTLINE, MINUTESELECT);
            }
            ispressed = 1;
        }

        if ((keypressed() == KEY_LEFT) & (!ispressed)) {
            if (cursorPosition != 0) {
                SetCursor(1, MINUTESELECT);
                LcdChar(' ');
                cursorPosition = 0;
                DrawArrow(HOURSELECT);
                SetCursor(FIRSTLINE, HOURSELECT);
            }
            ispressed = 1;
        }

        if ((keypressed() == KEY_UP) & (!ispressed)) {
            if (cursorPosition == 0) {
                ++hour;
                if (hour >= 24) hour = 0;
                ChangeNumbers(hour);
            } else {
                ++minute;
                if (minute >= 60) minute = 0;
                ChangeNumbers(minute);
            }
            ispressed = 1;
        }

        if ((keypressed() == KEY_DOWN) & (!ispressed)) {
            if (cursorPosition == 0) {
                --hour;
                if (hour < 0) hour = 23;
                ChangeNumbers(hour);
            } else {
                --minute;
                if (minute < 0) minute = 59;
                ChangeNumbers(minute);
            }
            ispressed = 1;
        }

        if ((keypressed() == KEY_OK) & (!ispressed)) {
            inTimeSelection = 0;
            ispressed = 1;
        }

        if ((ispressed) & (keypressed() == KEY_UNDEFINED)) {
            ispressed = 0;
        }
    }

    ClearDisplay();
    *hOutput = hour;
    *mOutput = minute;
    inTimeSelection = 0;
}
