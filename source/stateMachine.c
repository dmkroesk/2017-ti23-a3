//
// Created by gijsb on 14-3-2017.
//
#include <stdio.h>
#include <string.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "menu.h"
#include "alarm.h"
#include <time.h>
#include "rtc.h"
#include "realTime.h"
#include "InternetConnection.h"
#include "IPStream.h"
#include "LedDisplay.h"
#include "../include/display.h"
#include "../include/realTime.h"
#include "../include/radioStations.h"
#include "stateMachine.h"
#include "VolumeHandler.h"
#define  UPDATETIMER    60
u_char state = MAIN;
int UpdateInterval=UPDATETIMER;
void timeView(void);
void menu(void);
u_char returnState(void);

//loop that determines what to do with wich state.
//TODO: put here your new states and add a functions for it.
void stateLoop(){
    for(;;) {

        switch (state) {
            case MAIN:
                timeView();
                break;

            case MENUINIT:
                printf("\n%d", NutHeapAvailable());
                subOptionsESCCommand();
                printf("\n%d", NutHeapAvailable());
                state = MENU;
                break;

            case MENU:
                menu();
                break;

            default:
                printf("You're not suppose to be in here");
                break;
        }

        VolumeHandler();
        WatchDogRestart();

        NutSleep(1000);
    }
}


//Fucntion that views the time on the display
//TODO: add working time that refreshes every minute
void timeView(){
    if (keypressed() == KEY_OK) {
        state = MENUINIT;
    }
}

//function that redirects you to the menu.
void menu(){
    if(!showMenu()) {
        ClearDisplay();
        state = MAIN;
    }
}

u_char returnState(){
    return state;
}
