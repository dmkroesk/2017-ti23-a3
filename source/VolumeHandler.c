//
// Created by gijsb on 20-3-2017.
//

#include <stdio.h>
#include <dev/hd44780.h>
#include <dev/term.h>
#include <string.h>
#include <sys/timer.h>
#include <time.h>
#include <sys/timer.h>
#include "menu.h"
#include "display.h"
#include "keyboard.h"
#include "VolumeHandler.h"

int volume, keyPressed;

void VolumeUpdate(void);



//Volume is upside down because NUT/OS wants it
void VolumeDown()
{
    if(volume != 10) ++volume;
    VolumeUpdate();
}

//Volume is upside down because NUT/OS wants it
void VolumeUp()
{
    if(volume != 0) --volume;
    VolumeUpdate();
}

void SetVolume(int newVolume)
{
    if(newVolume >= 0 & newVolume <= 10)
    {
        volume = newVolume;
        VolumeUpdate();
    }
}

void VolumeInit()
{
    volume = 0;
    keyPressed = 1;
    VolumeUpdate();
}

void VolumeHandler()
{
    if((keypressed() == KEY_UP) & (!keyPressed))
    {
        VolumeUp();
        keyPressed = 1;
    }

    if((keypressed() == KEY_DOWN) & (!keyPressed))
    {
        VolumeDown();
        keyPressed = 1;
    }

    if(keypressed() == KEY_UNDEFINED) keyPressed = 0;

}

void VolumeIconUpdate()
{
    DrawSoundIcon(10 - volume);
}

void VolumeUpdate()
{
    int realVolume;

    if(volume == 10) realVolume = 255;
    else realVolume = (6*(volume));

    printf("\nVolume %d", realVolume);
    VsSetVolume(realVolume, realVolume);
    VolumeIconUpdate();
}

int getVolume()
{
    return 10 - volume;
}