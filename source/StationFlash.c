//
// Created by Sascha on 24-3-2017.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/flash.h"
#include "../include/flashSettings.h"
#include "../include/radioStations.h"
#define  MAX_STATIONS_ON_PAGE      (256/(sizeof(STATION)+sizeof(int)))
#define  MAX_PAGES  0x07
#define START_PAGE  0x02
#define MAX_STATIONS    MAX_STATIONS_ON_PAGE * (MAX_PAGES-START_PAGE)

int AStations = 0;           //THE AMOUNT OF STATIONS STORED
STATION allStations[MAX_STATIONS];  //THR STATIONS

void setAST(int ast)
{
    printf("AST Set");
    AStations = ast;
}

int addStation(STATION station)
{
    allStations[AStations] = station;
    AStations++;
    changeSetting("ast",AStations);
    saveStations();
    return 0;
}

int removeLastStation()
{
    AStations--;
    changeSetting("ast",AStations);
    return 0;
}

int addStream(char desc, char url, char path, int port)
{
    STATION station ={desc, url, path, port};
    addStation(station);
}

STATION * getStations()
{
    return allStations;
}

int saveStations()
{
    int succes = 0;
    int pages = (AStations/MAX_STATIONS_ON_PAGE);
    if(AStations%MAX_STATIONS_ON_PAGE!=0){pages++;}
    pages+= START_PAGE;
    int i = START_PAGE;
    int base = 0;
    for( i = START_PAGE; i<pages; i++)
    {
        STATION page[MAX_STATIONS_ON_PAGE];
        int j=0;
        for( j = 0; j<MAX_STATIONS_ON_PAGE; j++)
        {
            page[j]=allStations[base];
            if(0!=At45dbPageWrite(i,page,sizeof(page))){succes =-1;}
            base++;
        }
        base--;
    }
    return succes;
}

int loadStations()
{
    int succes = 0;
    AStations = getSetting("ast");
    printf("\n Stations Loaded: %i",AStations);
    if(AStations>0 && AStations<MAX_STATIONS)
    {
        int pages = (AStations/MAX_STATIONS_ON_PAGE);
        if(AStations%MAX_STATIONS_ON_PAGE!=0){pages++;}
        pages+= START_PAGE;
        int i = START_PAGE;
        int base = 0;
        for( i = START_PAGE; i<pages; i++)
        {
            STATION page[MAX_STATIONS_ON_PAGE];
            int j=0;
            for( j = 0; j<MAX_STATIONS_ON_PAGE; j++)
            {

                if(0!=At45dbPageRead(i,page,sizeof(page))){succes =-1;}
                allStations[base] = page[j];
                base++;
            }
            base--;
        }
    }

}



void printStations()
{
    int i =0;
    for(i=0; i<AStations;i++)
    {
        printf("\n Station %s",allStations[i].desc);
        printf("\n URL %s",allStations[i].url);
        printf("\n Path %s",allStations[i].path);
        printf("\n Port %i",allStations[i].port);
    }
}

void showPage(u_long pgn)
{
    unsigned char *pc = (unsigned char *) malloc(264);
    int idx;

    At45dbPageRead(pgn, (unsigned char *)pc, 264);
    for(idx = 0; idx < 264; idx++)
    {
        if( 0 == (idx % 32) )
            printf("\n");

        if( isalpha(pc[idx]) || isdigit(pc[idx]) )
        {
            printf("%c  ", pc[idx]);
        }
        else
        {
            printf("%02X ", pc[idx]);
        }
    }
    printf("\n");
    free(pc);
}





