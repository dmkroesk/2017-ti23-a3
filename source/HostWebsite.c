//
// Created by gijsb on 27-3-2017.
//
#include "../include/InternetConnection.h"
#include "../include/HostWebsite.h"
#include "../include/IPStream.h"
#include "../include/radioStations.h"
#include "../include/VolumeHandler.h"

#include <sys/timer.h>

//Thread for starting the web
THREAD(WebThread, arg){
    while(1)
    {
        FILE *stream;
        TCPSOCKET *sock = ConnectServer(80); //Makes a connection with the server

        stream = _fdopen((int) ((uptr_t) sock), "r+b");
        NutHttpProcessRequest(stream);
        printf("\nLooking for a connection....");

        NutTcpAccept(sock, 80); // Waits for client to connect.
        showParams(stream); //sends the website to the client.
        printf("\nConnection found!!");

        NutTcpCloseSocket(sock); //breaks connection with client
        fclose(stream); //Closes the stream between the client and server

        printf("\nConnection gone");

        printf("\n%d", NutHeapAvailable());

        NutSleep(1000);
    }
}

void startWebThread()
{
    NutThreadCreate("WebThread", WebThread, NULL, 512);
    NutThreadSetPriority(200);
    printf("\nWeb thread started");
}

int showParams(FILE *stream)
{
    fprintf(stream, "INFORMATION ABOUT RADIO \n\n");
    fprintf(stream, "Current stream playing: %s\n\n", getSelected());
    fprintf(stream, "Available streams\n");
    fprintf(stream, "Stream 1: %s\n", stations[0].desc);
    fprintf(stream, "Stream 2: %s\n", stations[1].desc);
    fprintf(stream, "Stream 3: %s\n\n", stations[2].desc);
    fprintf(stream, "Volume: %d\n",getVolume());

    return 0;
}
